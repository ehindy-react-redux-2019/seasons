import React from 'react'

type IProps = {
  message?: string
}

const Spinner = (props: IProps) => {
  return (
    <div className="ui active dimmer">
      <div className="ui big text loader">
        {props.message}
      </div>
    </div>
  );
};

Spinner.defaultProps = {
  message: "Loading..."
}

export default Spinner;
